package com.ywy.config;

/**
 * 全局配置
 * @author ywy
 * @version 1.0.0
 * @date 2021-04-06 16:34
 */
public class GlobalConfig {
    /**
     * 是否测试
     */
    public static final boolean isTest = false;

    /**
     * 上传bpmn文件路径
     */
    public static final String bpmnFileUploadPath = "E:\\boot-activiti\\bpmn\\"; // Windows
//    public static final String bpmnFileUploadPath = "/root/app/boot-activiti/bpmn/"; // Linux
}
